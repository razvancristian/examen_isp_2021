package sub2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Window extends JFrame implements ActionListener {
    private JTextArea textArea1;
    private JTextArea textArea2;
    private JButton button;
    private int m = 0;

    public Window() {
        this.setTitle("Window");
        this.setSize(500, 500);
        this.setLayout(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.textArea1 = new JTextArea();
        textArea1.setBounds(20, 20, 300, 40);
        this.textArea2 = new JTextArea();
        textArea2.setBounds(20, 80, 300, 40);
        this.button = new JButton();
        button.setBounds(20, 120, 20, 20);
        button.addActionListener(this);

        add(textArea1);
        add(textArea2);
        add(button);
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(button)) {
                int n = textArea1.getText().length();

                textArea2.setText("Number character:" + String.valueOf(n));

            }
        }
}

