import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCircle {
    @Test
    void testConstructor() {
        Circle c1 = new Circle(1.0);
        assertEquals(1.0, c1.getRadius());
        assertEquals("red", c1.getColour());
    }

    @Test
    void testOthervalue() {
        Circle c2 = new Circle(10);
        assertEquals(10.0, c2.getRadius());
    }

    @Test
    void testArea() {
        Circle c3 = new Circle(5.0);
        assertEquals(78.5, c3.getAria(), 0.1);
    }
}
