import java.util.Scanner;

public class Circle {
    private double radius = 1.0;
    private String colour = "red";

    public Circle() {

    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public double getAria() {
        return Math.PI * Math.pow(radius, 2);
    }

    public String getColour() {
        return colour;
    }
}
